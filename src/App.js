import React, { Component } from 'react'

import HomeRout from './views/home/HomeRoute'

class App extends Component {
  render() {
    return (
      <div className="App">
        <HomeRout />
      </div>
    )
  }
}

export default App
