import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/firestore'
import 'firebase/storage'

const config = {
  apiKey: "AIzaSyBtm0yx5MNaw_j0nKg_Hm6nZKRf8PBrS-M",
  authDomain: "aftersaleseviceofed.firebaseapp.com",
  databaseURL: "https://aftersaleseviceofed.firebaseio.com",
  projectId: "aftersaleseviceofed",
  storageBucket: "aftersaleseviceofed.appspot.com",
  messagingSenderId: "980217152428"
};
firebase.initializeApp(config);

if (!firebase.app.length) {
  firebase.initializeApp(config)
}

const db = firebase.firestore()
const auth = firebase.auth()
const storage = firebase.storage()

db.settings({
  timestampsInSnapshots: true
})

export default {
  db,
  auth,
  storage
}
