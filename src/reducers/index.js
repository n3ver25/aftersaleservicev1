import { combineReducers } from 'redux'
import { routerReducer as router } from 'react-router-redux'
import { reducer as form } from 'redux-form'

import products from './products'

export default combineReducers({
  router,
  form,
  products
})