import React from 'react'
import { connect } from 'react-redux'
import { lifecycle, compose } from 'recompose'

import ProductItem from './components/ProductItem'
import { findAll } from '../../action/products/index'

const products = [{
  id: 1,
  name: 'Product 1',
  coverImage: 'https://images.unsplash.com/photo-1517309561013-16f6e4020305?auto=format&fit=crop&w=750&q=80'
}, {
  id: 2,
  name: 'Product 2',
  coverImage: 'https://images.unsplash.com/photo-1517309561013-16f6e4020305?auto=format&fit=crop&w=750&q=80'
}]


const HomePage = () => {
  return (
    <div className="home">
      <section className="hero is-medium is-primary">
        <div className="hero-body">
          <div className="container">
            <h1 className="title">Ahoy Shop!</h1>
            <h2 className="subtitle">Shopping Website with React and Firebase</h2>
          </div>
        </div>
      </section> 
      <section className="section home-products">
        <div className="container">
          <div className="columns">
            <div className="column is-12">
              <h2 className="title">Our Products</h2>
            </div>
          </div>

          <div className="columns is-multiline">
            {products &&
              products.map(product => {
                const linkToDetail = `/products/${product.id}`

                return (
                  <div className="column is-4">
                    <ProductItem 
                    product={product} 
                    to={linkToDetail} 
                    />
                  </div>
                )
              })}
          </div>
        </div>
      </section>
    </div>
  )
}

const withLifecycle = lifecycle({
  componentDidMount() {
    const { dispatch } = this.props

    dispatch(findAll())
  }
})

export default compose(
  connect(),
  withLifecycle
 ) (HomePage)