import React from 'react'
import { BrowserRouter as Switch, Route } from 'react-router-dom';

import HomePage from './HomePage'

export default () => (
    <Switch>
      <Route exact path="/" component={HomePage} />
    </Switch>
)